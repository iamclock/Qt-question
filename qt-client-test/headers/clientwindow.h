#ifndef CLIENTWINDOW_H
#define CLIENTWINDOW_H


#include "ui_clientwindow.h"
#include <QStackedWidget>
#include <QFileDialog>
#include <QMessageBox>
#include <QMainWindow>
#include <QTcpSocket>
#include <QRegExp>
#include <QDebug>



class clientWindow : public QMainWindow, public Ui::clientWindow
{
    Q_OBJECT

public:
	clientWindow(QWidget *parent = 0);
	~clientWindow();
//    ~clientWindow();
	void setIP(QString);
	void setPort(QString);

private slots:
	void on_butChooseFile_clicked();
	void on_butSend_clicked();
	void on_butConnect_clicked();
	void catchRequest();
	void connected();
	void disconnected();
//	void readyStuff();
private:
//    Ui::clientWindow *ui;
	void setBlockInterface(bool);
	void loading();
	void sendFile(QString);
	bool sendData(QByteArray);
//	void checkLineEditWord();
	bool checkStringFromFile(QString);
	QTcpSocket *socket;
//	QValidator *valid1;
//	QValidator *valid2;
	quint16 port;
	QString ip;
};

#endif // CLIENTWINDOW_H
