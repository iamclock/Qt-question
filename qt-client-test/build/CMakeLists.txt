cmake_minimum_required(VERSION 2.8.12)

project(qt-client-r-test)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_PREFIX_PATH ~/Qt/5.6/gcc_64/lib/cmake ${CMAKE_PREFIX_PATH})
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")


# declaring files
set( SOURCES ../sources/client.cpp ../sources/clientwindow.cpp)
set( MOC_HEADERS ../headers/clientwindow.h)
set( UIS ../clientwindow.ui)
set( RESOURCES ../resources/res.qrc)
set(QT_LIBRARIES Qt5::Core Qt5::Gui Qt5::Network Qt5::Widgets)

# for building all Qt projects
#include( ${QT_USE_FILE} )
#add_definitions( ${QT_DEFINITIONS} )
#include_directories( ${CMAKE_BINARY_DIR} )

# using Qt meta-system (precompiler)
#QT5_ADD_RESOURCES( RES_SOURCES ${RESOURCES} )
#QT5_WRAP_UI( UI_HEADERS ${UIS} )
#QT5_WRAP_CPP( MOC_SRCS ${MOC_HEADERS} )

find_package(Qt5 REQUIRED COMPONENTS Core Widgets Gui Network)
#add_executable(${PROJECT_NAME} ${SOURCES} ${MOC_SRCS} ${RES_SOURCES} ${UI_HEADERS})
add_executable(${PROJECT_NAME} ${SOURCES} ${MOC_HEADERS} ${UIS} ${RESOURCES} )
# For Windows
#add_executable( ${PROJECT_NAME} WIN32 ${SOURCES} ${MOC_SRCS} ${RES_SOURCES} ${UI_HEADERS} )

# build it (link libraries)
target_link_libraries( ${PROJECT_NAME} ${QT_LIBRARIES} )
