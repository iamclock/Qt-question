#include "../headers/r-test-server.h"



rtserver::rtserver(QObject *parent) : QTcpServer(parent){
//	qDebug() << countMatchesInFile(QString("revolution"), QString("/tmp/11.txt")); // для отладки
	qsrand(QTime::currentTime().msec());
	connect(this, SIGNAL(newConnection()), SLOT(newConnection()));
}

void rtserver::newConnection(){
	while( hasPendingConnections() ){
		QTcpSocket *client = nextPendingConnection();
		QByteArray *buffer = new QByteArray();
		QString *fileName = new QString();
		qint32 *s = new qint32(0);
	
		fileNames.insert(client, fileName);
		buffers.insert(client, buffer);
		sizes.insert(client, s);
		qDebug() << "New client from:" << client->peerAddress().toString();
		connect(client, SIGNAL(readyRead()), this, SLOT(readyStuff()));
		connect(client, SIGNAL(disconnected()), this, SLOT(disconnected()));
	}
}

/*
void rtserver::incomingConnection(qintptr socketfd){
	while( hasPendingConnections() ){
		QTcpSocket *client = new QTcpSocket(this);
		client->setSocketDescriptor(socketfd);
		QByteArray *buffer = new QByteArray();
		QString *fileName = new QString();
		qint32 *s = new qint32(0);

		fileNames.insert(client, fileName);
		buffers.insert(client, buffer);
		sizes.insert(client, s);
		qDebug() << "New client from:" << client->peerAddress().toString();
		connect(client, SIGNAL(readyRead()), this, SLOT(readyStuff()));
		connect(client, SIGNAL(disconnected()), this, SLOT(disconnected()));
	}
}
*/

void rtserver::receiveFile(QString const fileName, QTcpSocket *const client){
	QByteArray *buffer = buffers.value(client);
	qint32 *s = sizes.value(client);
	QFile newFile(fileName);
	qint32 bytesWritten;

	qDebug() << "Is client ready? " << client->isOpen();
	if(*s == 0){
		*s = client->readAll().toInt();
		client->waitForReadyRead(2000);
	}
	bytesWritten = 0;
	if (newFile.open(QIODevice::WriteOnly)){
		while (client->bytesAvailable()){
//			qDebug() << "bytesAvailable:" << client->bytesAvailable();
			buffer->append(client->readAll());
			newFile.write(*buffer);
			bytesWritten += buffer->size();
			buffer->remove(0, buffer->size());
			client->waitForReadyRead(1000);
		}
		newFile.close();
	}else
		qDebug() << "File wasn't open";
//	emit fileReceived();
}

void rtserver::readyStuff(){
	QTcpSocket *client = (QTcpSocket *)sender();
	QString *fileName = fileNames.value(client);
	QString word;

	if(fileName->size() == 0){
		fileName->push_back("/tmp/");
		fileName->push_back( (QString::number(qrand()%(500000-9)+10)) );
		fileName->push_back(".txt");
	}
	qDebug() << *fileName;
	qDebug() << "word Size: " << word.size();
	if(word.size() == 0){
		word = QString::fromUtf8(client->readLine().trimmed());
		client->waitForReadyRead(2000);
	}
	qDebug() << "word: " << word;
	receiveFile(*fileName, client);
	client->write( QByteArray::number( countMatchesInFile(word, *fileName) ) );
	client->waitForBytesWritten();
	//qDebug() << "matches: " << countMatchesInFile(word, fileName);//QString(countMatchesInFile(word, fileName));
	qDebug() << "readyStuff";
}

void rtserver::disconnected(){
	QTcpSocket *client = (QTcpSocket*)sender();
	QString *fileName = fileNames.value(client);
	QByteArray *buffer = buffers.value(client);
	qint32 *s = sizes.value(client);
	

	qDebug() << "Client disconnected:" << client->peerAddress().toString();
	client->deleteLater();
	if(QFile::exists(*fileName))
		QFile::remove(*fileName);
	delete fileName;
	delete buffer;
	delete s;
//	clients.remove(client);
	//удалить, закрыть файл
}

quint32 rtserver::countMatchesInFile(QString word, QString fileName){
	QString s;
	QFile file(fileName);
	QTextStream fileStream(&file);
	int matches = 0;
	if( file.open(QIODevice::ReadOnly) ){
		while( !(fileStream.atEnd()) ){
			fileStream >> s; // 
			if(s.contains(word, Qt::CaseInsensitive)){
				++matches;
				qDebug() << s;
			}
		}
		file.close();
	}
//	qDebug() << "matches = " << matches;
//	qDebug() << "i = " << i;
	return matches;
}