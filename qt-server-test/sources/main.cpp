#include <QCoreApplication>
#include <QTextStream>
#include <QDebug>
#include "../headers/r-test-server.h"


int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	rtserver *server = new rtserver();
	quint16 port = 0xbabe; // 47806
//	QTextStream qin(stdin), qout(stdout); // qout Не работает
//	qout << "Enter port: ";
//	qin >> port;
//	qDebug() << port;
//	Проверка порта, что не перекрывает системные
	bool isSuccess = server->listen(QHostAddress::Any, port);
	if( !isSuccess ){
		/*
		QString error("Could not listen on port ");
		error += QString::number(port);
		return 0;
		*/
		qFatal("Could not listen on port");
	}
//	qout << "success";
	qDebug() << "success";
	return a.exec();
}
